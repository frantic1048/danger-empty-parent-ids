import { danger, warn } from "danger";
import { execaPromise } from "./util.cjs";

const checkMergeCommits = async () => {
  const execa = await execaPromise;
  const $ = execa.$;

  console.log("Examining commits");

  for (const commit of danger.gitlab.commits) {
    const gitParentIds = (
      await $`git show -s --pretty=format:%p ${commit.short_id}`
    ).stdout
      .toString()
      .split(" ");

    console.log(`
commit: ${commit.short_id}
\tparent_ids(danger): ${commit.parent_ids.join(", ")}
\tparent_ids(git): ${gitParentIds.join(", ")}
`);

    warn(
      `${commit.short_id}: parent_ids(danger): ${commit.parent_ids.join(
        ", "
      )}, parent_ids(git): ${gitParentIds.join(", ")}`
    );
  }
};

const main = async () => {
  await checkMergeCommits();
};

void main();
